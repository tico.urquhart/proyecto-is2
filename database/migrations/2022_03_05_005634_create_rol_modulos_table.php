<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('se_rol_modulo', function (Blueprint $table) {
            $table->id();
            $table->boolean("acceso");
            $table->foreignId("id_rol");
            $table->foreign("id_rol")->references("id")->on("se_roles");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('se_rol_modulo');
    }
}
