<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER tr_usuario_proyecto AFTER INSERT ON pr_proyecto
        FOR EACH ROW INSERT INTO proyectousuario (id, id_proyecto, id_users, created_at, updated_at) VALUES (NULL, NEW.id, "1", NULL, NULL) ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_usuario_proyecto`');
    }
}
