<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrolTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("CREATE TRIGGER tr_rol_nuevo AFTER INSERT ON se_roles
            FOR EACH ROW INSERT INTO se_rol_modulo (acceso, id_rol, id_modulo)
            SELECT '0',NEW.id, se_modulos.id FROM se_modulos WHERE se_modulos.id NOT IN  (select se_rol_modulo.id_modulo from se_rol_modulo WHERE se_rol_modulo.id_rol = NEW.id )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_rol_nuevo` ');
    }
}
