<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectousuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectousuario', function (Blueprint $table) {
            $table->id();
            $table->foreignId("id_proyecto");
            $table->foreign("id_proyecto")->references("id")->on("pr_proyecto");
            $table->foreignId("id_users");
            $table->foreign("id_users")->references("id")->on("users");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectousuario');
    }
}
