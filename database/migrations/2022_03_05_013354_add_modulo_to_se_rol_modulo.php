<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddModuloToSeRolModulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('se_rol_modulo', function (Blueprint $table) {
            $table->foreignId("id_modulo");
            $table->foreign("id_modulo")->references("id")->on("se_modulos");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('se_rol_modulo', function (Blueprint $table) {
            //
        });
    }
}
