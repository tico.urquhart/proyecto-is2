<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSeRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_se_roles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId("id_rol");
            $table->foreign("id_rol")->references("id")->on("se_roles");
            $table->foreignId("id_users");
            $table->foreign("id_users")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_se_roles');
    }
}
