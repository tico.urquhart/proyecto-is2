<?php
use App\Models\Roles;
use App\Models\Modulos;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\ProyectoController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\ModuloControlador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

#Rutas publicas
Route::post('/login',[AuthController::class,'login']);



#Rutas protegidas
Route::group(['middleware'=>['auth:sanctum']], function () {
    /*Proteger las siguientes rutas, todas las rutas escritas dentro de este closure, estaran protegidas para solamente los usuarios logueados*/
    // Obtener modulos de manera vieja
    // Route::get('/modulos',function (){
    //     return Modulos::all();
    // });

    ## USUARIO
    ##PANEL
    Route::get('/usuario/{nombre?}',function ($nombre="") { return response(["cod"=>"00","mensaje"=>"Realizado con exito.","lista"=>UsuarioController::listar($nombre)],201);});
    ##ABM
    Route::post('/usuario',[UsuarioController::class,'nuevo']);
    Route::delete('/usuario/{id}',[UsuarioController::class,'eliminar']);
    Route::put('/usuario/{id}',[UsuarioController::class,'modificar']);
    ##EXTRA
    Route::put('/usuarioPass/{id}',[UsuarioController::class,'cambiarPass']);
    ##obtener usuario pasandole id como parametro
    Route::get('/usuario/id/{id}',[UsuarioController::class,'obtenerUser']);

    ## ROL
    ##PANEL
    Route::get('/rol/{desc?}',function ($desc="") { return response(["cod"=>"00","mensaje"=>"Realizado con exito.","lista"=>RolController::listar($desc)],201);});
    ##ABM
    Route::post('/rol',[RolController::class,'nuevo']);
    Route::delete('/rol/{id}',[RolController::class,'eliminar']);
    Route::put('/rol/{id}',function (Request $request,$id) { return RolController::modificar($request,$id); });
    // Route::get('/rol/id/{id}',[RolController::class,"obtenerRoles"]);


    ##MODULOS
    ##PANEL
    Route::get('/modulo/{descripcion?}',function ($nombre="") { return response(["cod"=>"00","mensaje"=>"Realizado con exito.","lista"=>ModuloControlador::listar($nombre)],201);} );
    ##ABM
    Route::post('/modulo',[ModuloControlador::class,'nuevo']);
    Route::delete('/modulo/{id}',[ModuloControlador::class,'eliminar']);
    Route::put('/modulo/{id}',[ModuloControlador::class,'modificar']);
    ##EXTRA
    ##obtener usuario pasandole id como parametro


    ## REFERENTE A LA SESION DEL USUARIO

    Route::post('/logout',[AuthController::class,'logout']);

    Route::get('/logueadoModulos',function (){ return ModuloControlador::listar_rol(Auth::id());});

    Route::get('/renew',function (){ return response( ((Auth::check())?["cod"=>"00", "mensaje"=> "logueado correctamente" , "usuario"=> Auth::user()] :["cod"=>"00","mensaje"=>"deslogueado"]) ) ; });


    # Listar,nuevo,modificar,eliminar Modulo

    ##PROYECTO
    ##PANEL
    Route::get('/proyecto/{descripcion?}',function ($descripcion="") { return response(["cod"=>"00","mensaje"=>"Realizado con exito.","lista"=>ProyectoController::listar($descripcion)],201);});
    ##ABM
    Route::get('/proyecto/id/{id}',[ProyectoController::class,'obtenerProyecto']);
    Route::post('/proyecto',[ProyectoController::class,'nuevo']);
    Route::put('/proyecto/{id}',[ProyectoController::class,'modificar']);
    Route::put('/proyecto/estado/{id}',[ProyectoController::class,'modificarEstadoProyecto']);
    Route::delete('/proyecto/{id}',[ProyectoController::class,'eliminar']);
    ##EXTRA

    


});
