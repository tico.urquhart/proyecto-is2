<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolModulo extends Model
{
   public $table ='se_rol_modulo';

    use HasFactory;
}
