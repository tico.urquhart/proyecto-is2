<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modulos extends Model
{
    public $table = 'se_modulos';

    use HasFactory;

    protected $fillable = [
        'descripcion'
    ];
}
