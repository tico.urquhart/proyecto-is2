<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    use HasFactory;
    public $table = 'pr_proyecto';

    protected $fillable = [
        'descripcion',
        'observacion',
        'estado',
    ];


}
