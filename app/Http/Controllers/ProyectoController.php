<?php

namespace App\Http\Controllers;

use App\Models\Proyecto;
use Illuminate\Http\Request;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public static function listar($desc){
        return Proyecto::select("id","descripcion","observacion","estado")->where('descripcion', 'LIKE', "%{$desc}%")->get();
    }

    public static function nuevo(Request $peticion){
        $campos = $peticion->validate([
            'descripcion'=>'required|string',
            'observacion'=>'string',
        ]);
        $proyecto = Proyecto::create([
            'descripcion'=>$campos['descripcion'],
            'observacion'=>$campos['observacion'],
        ]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Creacion de proyecto realizada con exito",
        ];
        return response($respuesta,201);
    }

    public static function modificar(Request $peticion,$id){
        $proyecto = Proyecto::find($id);
        $campos = $peticion->validate([
            'descripcion'=>'required|string',
            'observacion'=>'string',
            ''
        ]);
        $proyecto->update(['descripcion'=>$campos['descripcion'],'observacion'=>$campos['observacion']]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Modificacion realizada con exito",
        ];

        return response($respuesta,201);
    }

    public static function obtenerProyecto($id){
        $usuario = Proyecto::find($id);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Proyecto Obtenido Correctamente",
            'usuario'=>$usuario
        ];
        return response($respuesta,201);
    }
    public static function modificarEstadoProyecto(Request $peticion,$id){
        $proyecto = Proyecto::find($id);
        $campos = $peticion->validate([
            'estado'=>'string',
        ]);
        if($proyecto->estado !='ELIMINADO' ){
            $proyecto->update(['estado'=>$campos['estado']]);
            $respuesta = [
                "cod"=>"00",
                'message'=>"Modificacion estado realizada con exito",
            ];
        }else{
            $respuesta = [
                "cod"=>"01",
                'message'=>"No me rompas los huevos :V",
            ];
        }
        return response($respuesta,201);
    }

    public static function eliminar($id){
        $proyecto = Proyecto::find($id);

        if($proyecto->estado !='ELIMINADO' ){
            $proyecto->update(['estado'=>'ELIMINADO']);
        }
        $respuesta = [
            "cod"=>"00",
            'message'=>"Modificacion estado realizada con exito"  ,
        ];
        return $respuesta;
    }
    public static function modificarUsuario(Request $peticion,$id){

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
