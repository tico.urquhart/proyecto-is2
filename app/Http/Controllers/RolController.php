<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class RolController extends Controller
{
    public function nuevo(Request $peticion){
        $campos = $peticion->validate([
            'descripcion'=>'required|string',
            // 'observacion'=>'string',
        ]);
        $rol = Roles::create([
            'descripcion'=>$campos['descripcion'],
            // 'observacion'=>$campos['observacion'],
        ]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Creacion de rol realizada con exito",
        ];
        return response($respuesta,201);
    }

    public function modificar(Request $peticion,$id){
        $rol = Roles::find($id);
        $campos = $peticion->validate([
            'descripcion'=>'required|string',
            // 'observacion'=>'string',
        ]);
        $rol->update(['descripcion'=>$campos['descripcion']]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Modificacion realizada con exito",
        ];

        return response($respuesta,201);
    }

    public function eliminar($id){
        $rol = Roles::find($id);
        $rol->delete();

        $respuesta = [
            "cod"=>"00",
            'message'=>"Eliminado con exito",
        ];
        return $respuesta;
    }

    public static function listar($desc){
        return Roles::select("id","descripcion")->where('descripcion', 'LIKE', "%{$desc}%")->get();
    }

}
