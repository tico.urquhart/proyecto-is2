<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserSeRoles;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;


class UsuarioController extends Controller
{
    public function nuevo(Request $peticion){
        $campos = $peticion->validate([
            'nombre'=>'required|string',
            'email'=>'required|string',
            'pass'=>'required|string|confirmed',
            'id_rol'=>'required|string'
        ]);
        $usuario = User::create([
            'name'=>$campos['nombre'],
            'email'=>$campos['email'],
            'password'=>bcrypt($campos['pass']),
            // 'id_rol'=>'1'
        ]);
        $rol = UserSeRoles::create([
            'id_users' => $usuario->id,
            // 'id_rol' => $campos['id_rol'] #produccion
            'id_rol' => "1" #test
        ]);
        
        $respuesta = [
            "cod"=>"00",
            'message'=>"Creacion de usuario realizada con exito",
            'usuario'=>$usuario,
        ];
        return response($respuesta,201);
    }

    public function modificar(Request $peticion,$id){
        $usuario = User::find($id);
        $campos = $peticion->validate([
            'nombre'=>'string',
            'email'=>'string',
        ]);
        $usuario->update(['name'=>$campos['nombre'],'email'=>$campos['email']]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Modificacion realizada con exito",
        ];

        return response($respuesta,201);
    }

    public function cambiarPass(Request $peticion,$id){
        $usuario = User::find($id);
        $campos = $peticion->validate([
            'pass'=>'required|string|confirmed',
        ]);
        $usuario->update(['password'=>bcrypt($campos['pass'])]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Cambio de pass realizado con exito",
        ];
        return response($respuesta,201);
    }

    public function eliminar($id){
        $usuario = User::find($id);
        $userRoles = UserSeRoles::where("id_users",$id)->delete();
        // $userRoles->delete();
        $userRoles;
        $usuario->delete();
        
        $respuesta = [
            "cod"=>"00",
            'message'=>"Eliminado con exito",
        ];
        return response($respuesta,201);
    }
    //modificar la funcion de listar 

    public static function listar($nombre){
        return User::select("users.id", "users.name", "users.email", "se_roles.descripcion as rol")
        ->join("user_se_roles","user_se_roles.id_users","=","users.id")
        ->join("se_roles","se_roles.id","=","user_se_roles.id_rol")
        ->where('users.name', 'LIKE', "%{$nombre}%")->get() ;
    }

    public function obtenerUser($id){
        $usuario = User::find($id);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Usuario Obtenido Correctamente",
            'usuario'=>$usuario
        ];
        return response($respuesta,201);
    }

    ##METODOS REFERENTES AL LOGUEO

    public function logout(Request $request){
        auth()->user()->tokens()->delete();
        return [
            "cod"=>"00",
            'message'=>"deslogueado"
        ];
    }

    public function login(Request $peticion){
        $campos = $peticion->validate([
            'email'=>'required|string',
            'pass'=>'required|string',
        ]);
        #Verificar existencia del correo recibido
        $usuario = User::where('email',$campos['email'])->first();
        if(!$usuario || !Hash::check($campos['pass'],$usuario->password)){
            return response([
                "cod"=>"01",
                'message'=>"Error, credenciales invalidas"
            ],201);
        }

        $token = $usuario->createToken('tokenInicio')->plainTextToken;
        $respuesta = [
            "cod"=>"00",
            'message'=>"Logueo Correcto",
            'usuario'=>$usuario,
            'token' => $token
        ];
        return response($respuesta,201);
    }

}
