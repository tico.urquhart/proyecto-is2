<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RolModulo;
use App\Models\Modulos;

class ModuloControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public static function listar_rol($id_rol){
        return Modulos::select('se_modulos.id','descripcion')
                ->join('se_rol_modulo','se_rol_modulo.id_modulo','=','se_modulos.id')
                ->where(['se_rol_modulo.id_rol'=>$id_rol, 'acceso'=>1])
                ->get();
    }
    public function obtenerModulos(){
        $modulos = Modulos::all();
        $respuesta = [
            "cod"=>"00",
            "message"=>"modulos encontrados correctamente",
            "modulos" => $modulos
        ];
        return response($respuesta,201);
    }

    public function nuevo(Request $peticion){
        $campos = $peticion->validate([
            'descripcion'=>'required|string',
        ]);
        // return $campos['descripcion'];
        $modulo = Modulos::create([
            'descripcion'=>$campos['descripcion']
        ]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Cracion del Modulo creado con exito",
            'usuario'=>$modulo,
        ];
        return response($respuesta,201);
    }


    public function modificar(Request $peticion,$id){
        $modulo = Modulos::find($id);
        $campos = $peticion->validate([
            'descripcion'=>'string'
        ]);
        $modulo->update(['descripcion'=>$campos['descripcion']]);
        $respuesta = [
            "cod"=>"00",
            'message'=>"Modificacion realizada con exito",
        ];

        return response($respuesta,201);
    }

    public function eliminar($id){
        $modulo = Modulos::find($id);
        $modulo->delete();
        $respuesta = [
            "cod"=>"00",
            'message'=>"Eliminado con exito",
        ];
        return response($respuesta,201);
    }

    public static function listar($desc){
        return Modulos::select("id","descripcion")->where('descripcion', 'LIKE', "%{$desc}%")->get();
    }

}
