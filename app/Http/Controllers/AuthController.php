<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserSeRoles;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    public function registro(Request $peticion){
        $campos = $peticion->validate([
            'nombre'=>'required|string',
            'email'=>'required|string',
            'pass'=>'required|string|confirmed',
        ]);
        $usuario = User::create([
            'name'=>$campos['usuario'],
            'email'=>$campos['email'],
            'password'=>bcrypt($campos['pass']),
            // 'id_rol'=>'1'
        ]);
        $rol = UserSeRoles::create([
            'id_rol'=>'1',
            'id_user'=> $usuario->id
        ]);
        // creo que posiblemente lo mejor seria al crear un usuario crear automaticamente la relacion en la tabla
       // user_se_roles

        $token = $usuario->createToken('tokenInicio')->plainTextToken;
        $respuesta = [

            'usuario'=>$usuario,
            'token' => $token
        ];
        return response($respuesta,201);
    }

    public function login(Request $peticion){
        $campos = $peticion->validate([
            'email'=>'required|string',
            'pass'=>'required|string',
        ]);
        #Verificar existencia del correo recibido
        $usuario = User::where('email',$campos['email'])->first();
        if(!$usuario || !Hash::check($campos['pass'],$usuario->password)){
            return response([
                "cod"=>"01",
                'message'=>"Error, credenciales invalidas"
            ],201);
        }

        $token = $usuario->createToken('tokenInicio')->plainTextToken;
        $respuesta = [
            "cod"=>"00",
            'message'=>"Logueo Correcto",
            'usuario'=>$usuario,
            'token' => $token
        ];
        return response($respuesta,201);
    }
    public function logout(Request $request){
        auth()->user()->tokens()->delete();
        return [
            "cod"=>"00",
            'message'=>"deslogueado"
        ];
    }
}
